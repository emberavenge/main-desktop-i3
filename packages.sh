#!/bin/bash

sudo pacman -S --noconfirm xorg lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings i3 lxappearance nitrogen alacritty picom dmenu rofi  nemo flatpak python-requests htop pacman-contrib playerctl pavucontrol python-dbus dunst archlinux-wallpaper awesome-terminal-fonts ttf-font-awesome xorg-fonts-misc mpv flameshot
sudo pacman -R --noconfirm i3lock
pikaur -S --noconfirm bumblebee-status betterlockscreen
sudo systemctl enable lightdm
